-- Taux d’acceptation des proposition de réalisation mission

SELECT avg("dm"."f_acceptance"."rate_of_acceptance") AS "avg", CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) as Calendrier
FROM "dm"."f_acceptance"
LEFT JOIN "dm"."d_calendar" "d_calendar__via__sid_d_calenda" ON "dm"."f_acceptance"."sid_d_calendar" = "d_calendar__via__sid_d_calenda"."sid_calendar"
GROUP BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT)
ORDER BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) ASC