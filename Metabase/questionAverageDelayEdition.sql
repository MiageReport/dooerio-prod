-- Délai moyen d'édition de facture

SELECT avg("dm"."f_invoice"."edition_delay") AS "avg", CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' ||  CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) as Calendrier
FROM "dm"."f_invoice"
LEFT JOIN "dm"."d_calendar" "d_calendar__via__sid_d_calenda" ON "dm"."f_invoice"."sid_d_calendar_start" = "d_calendar__via__sid_d_calenda"."sid_calendar"
GROUP BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' ||  CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT)
ORDER BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) ASC
