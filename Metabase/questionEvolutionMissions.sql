-- Courbe d’évolution du nombre de missions proposées

SELECT sum("dm"."f_proposition"."number_of_proposition") AS "sum", CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / '||CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) AS Calendrier 
FROM "dm"."f_proposition"
LEFT JOIN "dm"."d_calendar" "d_calendar__via__sid_d_calenda" ON "dm"."f_proposition"."sid_d_calendar" = "d_calendar__via__sid_d_calenda"."sid_calendar"
GROUP BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / '||CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT)
ORDER BY CAST("d_calendar__via__sid_d_calenda"."day" AS TEXT) || ' / ' || CAST("d_calendar__via__sid_d_calenda"."month" AS TEXT) || ' / '||CAST("d_calendar__via__sid_d_calenda"."year" AS TEXT) ASC