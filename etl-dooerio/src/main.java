import dooerio.etl_dooerio_0_1.etl_dooerio;

import java.time.LocalDateTime;

/**
 * Created by maxim on 02/03/2017.
 */
public class main
{
    public static void main(String[] args)
    {
        System.out.println("=== Start running Dooer.io ETL process ===");
        System.out.println("Start: " + LocalDateTime.now());

        etl_dooerio etl_dooerio = new etl_dooerio();
        etl_dooerio.runJob(new String[]{});

        System.out.println("End: " + LocalDateTime.now());
        System.out.println("=== End running Dooer.io ETL process ===");


    }
}
