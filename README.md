<img src="https://gitlab.com/MiageReport/dooerio-prod/raw/master/resources/img/dooer.png">
# COE's dashboard

## Description

Le **dashboard du CEO** est un projet d’informatique décisionnel. Il doit permettre de **visualiser rapidement quelques indicateurs clés**.

## Indicateurs

* Courbe d’évolution du nombre de missions proposées
* Taux d’acceptation des proposition de réalisation mission
* Délais moyen d’édition de facture

## Prestataire

<img src="https://gitlab.com/MiageReport/dooerio-prod/raw/master/resources/img/mr.png" width="150px">


**Chef de projet**: Aurélien ROUSSEAU

**Expert dataviz**: Cyril FERLICOT

**Analyste BI**: Maxime PIQUET

Le dashboard est disponible à cette adresse: <a href="https://dooerio.herokuapp.com">https://dooerio.herokuapp.com</a>


# Flux du projet

<img src="https://gitlab.com/MiageReport/dooerio-prod/raw/master/resources/img/20170218_MR_SchemaBI.png">

### 1. Récupération des données externes ###

Nous faisons l'extraction des données des projets _Factures_ et _Missions_ via l'ETL Talend.
Nous chargeons ensuite les données dans notre DataWarehouse (DWH).

Très peu de transformations sont faites durant cette étape ( _1 pour 1_ ).

### 2. Transformation de la donnée ###

Nous utilisons Talend pour extraire les données du DWH, les transformer, puis pour les charger dans notre DataMart (DM).

### 3. Restitution de la donnée ###

Nous utilisons Metabase pour bénéficier d'un outil de visualisation puissant.

Metabase va analyser les données du DM pour proposer un ensemble de critères afin de définir des règles très facilement.
Nous avons donc la possibilité de visualiser de la donnée sans devoir posséder des connaissances en développement ou requêtage BDD.

## Techniques

### 1. Création de la base

Pour créer la base de donnée PostGreSQL, il suffit de lancer le script **database_bi_dooerio.sql**

### 2. ETL

Pour lancer l'ETL, il faut executer les scripts batchs de Talend **etl_dooerio_run.sh** (UNIX) ou **etl_dooerio_run.bat** (Windows), inclus dans l'archive ZIP **etl_dooerio_0.1zip**

### 3. Dataviz

L'outil de dataviz Metabase permetettant de construire des questions sous forme de requêtes SQL, les trois **KPI** demandés peuvent être récrés grâces aux scripts SQL du dossier **Metabase**